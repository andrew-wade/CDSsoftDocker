LIGO CDS software packaging installed in a Debian docker.  

This docker is based on Jamie Rollins's instructions that can be found here: https://git.ligo.org/cds-packaging/docs/wikis/home

Experimental.

Currently fails at Configuring Kerberos Authentication stage, this requires user interaction and install stalls during docker build.
