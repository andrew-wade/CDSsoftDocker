FROM debian:jessie

MAINTAINER Andrew Wade <awade@ligo.caltech.edu>

# This dockerfile is trying to reproduce a standard install of the CDS software
# package distributed by the LIGO people (aka J. Rollins).
#
# It should perform two important functions: one, to provide a test platform
# with pre-build environment with all the usual LIGO CDS tools that *just works*
# (in a OS agnostic way); and, two, give an explicit reference providing exact
# syntax for how it might be installed on a fresh debian box.
#
# It can be handy to have a pre-build pristine working system to test if a
# problem is reproducible.  It might also be useful for deploying particular
# tools for short times when the user doesn't want to modify the host system.
#
# On this second point it is often very educational to look at Dockerfiles to
# see what tricks were exactly done to reach a working build.  Dockerfiles list
# steps explicitly and there is nothing left to the imagination or know how.
#
# Instructions on how to install are here:
# https://git.ligo.org/cds-packaging/docs/wikis/home
#

# Set the working directory to /app
WORKDIR /builddir


# Copy config folder contents into builddir for configuring apt-get
ADD ./config /builddir/config/

# These are needed for hardware interface with modbus over TCP/IP
# Commented out because unneeded for basic build.
#EXPOSE 5064
#EXPOSE 5064/udp
#EXPOSE 5065
#EXPOSE 5065/udp

# These were needed for building EPICS and its modules from source
# commented out becuase probably don't need for packaged cdssoft stuff
#ENV EPICS_HOST_ARCH="linux-x86_64" \
#    EPICS_ROOT="/opt/epics" \
#    EPICS_BASE="/opt/epics/base" \
#    EPICS_BASE_BIN="/opt/epics/base/bin/linux-x86_64" \
#    EPICS_BASE_LIB="/opt/epics/base/lib/linux-x86_64"

RUN apt-get update -q \
    && mv /builddir/config/cdssoft.list /etc/apt/sources.list.d/cdssoft.list \
    && mv /builddir/config/lscsoft.list /etc/apt/sources.list.d/lscsoft.list \
    && apt-get update -q \
    && apt-get --yes --force-yes install \
	lscsoft-archive-keyring \
    && apt-get update -q
#    && apt-get clean

# Splitting the run to shorten build time when first part remains unchanged
RUN apt-get update -q \
    && apt-get install -y --force-yes \
        cds-workstation
#    && apt-get clean

# Editing tools for interactive mode (disable for thinner build)
RUN apt-get --yes install \
     vim

# This was previously used to launch an instance for modbus over TCP/IP server
#CMD ["/opt/epics/modules/modbus/bin/linux-x86_64/modbusApp", "/home/modbus/iocBoot/acromag.cmd"]
